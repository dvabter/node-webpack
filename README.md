# Getting started with Node.js, React.js, webpack and babel

- source: [webpack-getting-started](https://webpack.js.org/guides/getting-started/)
- source: [react-from-scratch-using-webpack](https://medium.freecodecamp.org/part-1-react-app-from-scratch-using-webpack-4-562b1d231e75)
- source: [bad-babel-dependencies](https://stackoverflow.com/questions/49182862/preset-files-are-not-allowed-to-export-objects)
