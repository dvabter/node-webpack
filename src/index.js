import _ from 'lodash';

import React from "react";
import ReactDOM from "react-dom";

const Index = () => {
  const hello_my_friend = _.reverse([
    "..............\\.............\\..",
    "............\\..............(",
    "..........''...\\.......... _.·´",
    ".........\\.................'...../",
    "........('(...´...´.... ¯~/'...')",
    "........../'/.../..../......./¨¯\\",
    "............./´¯/'...'/´¯¯`·¸",
    ".................../..../",
    "....................,/¯../",
    "....................../´¯/)",
    "========== hello world ==========="
  ]);
  const hello = _.join(hello_my_friend, "\n");
  return (<div><pre>{hello}</pre></div>);
};

ReactDOM.render(<Index />, document.getElementById("index"));
