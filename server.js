const express = require('express');
const path = require('path');
const app = express();

app.set('port', 8080);

const wwwdir = path.join(__dirname, 'dist');

app.use(express.static(wwwdir));

const server = app.listen(app.get('port'), function () {
  console.log('The server is running on http://localhost:' + app.get('port'));
  console.log('The server is serving:' + wwwdir);
});
