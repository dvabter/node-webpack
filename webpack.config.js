const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");

const srcdir = path.resolve(__dirname, 'src');
const outdir = path.resolve(__dirname, 'dist');

const html_plugin = new HtmlWebPackPlugin({
  template: path.resolve(srcdir, 'index.html'),
  filename: path.resolve(outdir, 'index.html'),
});


module.exports = {
  entry: path.resolve(srcdir, 'index.js'),
  output: {
    filename: 'main.js',
    path: outdir
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
      }
    ]
  },
  plugins: [html_plugin]
};
